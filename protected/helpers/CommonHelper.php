<?php
/**
 * Created by PhpStorm.
 * User: misantron
 * Date: 21.09.2014
 * Time: 20:26
 */

class CommonHelper
{
    const SECRET_KEY = '97342215053497a750ced94.27258802';

    /**
     * @param $cypher
     * @param $string
     * @return string
     */
    public static function getHash($string, $cypher = 'ripemd256')
    {
        return hash($cypher, $string . self::SECRET_KEY); // len 64
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateString($length = 20)
    {
        $randChars = array();
        $alphabet = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $alphabetSize = strlen($alphabet);
        while(--$length+1){
            $randChars[] = $alphabet[mt_rand(0, $alphabetSize-1)];;
        }
        shuffle($randChars);
        return implode('', $randChars);
    }

    /**
     * @param array $list
     * @return array
     */
    public static function toArray($list)
    {
        $arrayList = [];
        foreach ($list as $val) {
            $arrayList[] = $val->getAttributes();
        }
        return $arrayList;
    }
}