<?php

/**
 * Class UserController
 */
class UserController extends BaseController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('*'),
                'actions'=>array('login', 'registration', 'list'),
            ),
            array('allow',
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
        $list = User::model()->findAll();

        $this->setPageTitle('Пользователи');

        $this->render('list',array('list'=>CommonHelper::toArray($list)));
    }

    public function actionRegistration()
    {
        if(!Yii::app()->user->isGuest){
            $this->redirect('/user/list');
            Yii::app()->end();
        }

        $this->layout = '//layouts/empty';

        $this->setPageTitle('Регистрация');

        $form = new LoginForm(User::EVENT_REGISTRATION);
        $formId = 'registration-form';

        $this->ajaxValidation($form, $formId);

        if(isset($_POST['LoginForm'])){
            $form->attributes = $_POST['LoginForm'];
            if($form->validate()){
                $model = new User;
                $model->email = $form->username;
                $model->password = CommonHelper::getHash($form->password);
                $model->name = CommonHelper::generateString();
                $model->save();
                $this->redirect('/user/list');
            }
        }

        $this->render('registration', array(
            'form' => $form,
            'formId' => $formId,
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect('/user/login');
    }

    public function actionLogin()
    {
        $model=new LoginForm;

        $this->layout = '//layouts/empty';
        $this->setPageTitle('Вход');

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
                $this->redirect('/user/list');
        }
        // display the login form
        $this->render('login',array('form'=>$model));
    }
}