<?php

/**
 * Class MessageController
 */
class MessageController extends BaseController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('*'),
                'actions'=>array('list'),
            ),
            array('allow',
                'users'=>array('@'),
                'actions'=>array('send', 'delete'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionList()
    {
        $toUserId = (int)$_GET['id'];
        $model = new Message();
        $formId = 'message-form';

        $this->setPageTitle('Сообщения');

        $list = Message::model()->toUser($toUserId)->findAll();

        $this->render('list', array(
            'toUserId' => $toUserId,
            'model' => $model,
            'formId' => $formId,
            'list' => $list
        ));
    }

    public function actionSend()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $model = new Message();
            $model->attributes = $_POST['Message'];
            if($model->validate() && $model->save()){
                echo $this->renderPartial('_item', array('item' => $model));
                Yii::app()->end();
            }
        }
        throw new CHttpException(400,'Неправильный запрос');
    }

    public function actionDelete()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $id = (int)$_POST['id'];
            $model = Message::model()->findByPk($id);
            if(isset($model)){
                $model->delete();
                Yii::app()->end();
            }
        }
        throw new CHttpException(400,'Неправильный запрос');
    }
}