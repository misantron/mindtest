<?php

class MessageForm extends CFormModel
{
    public $to_user_id;
    public $message;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('to_user_id, message', 'required'),
            // rememberMe needs to be a boolean
            array('to_user_id', 'integer'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'message'=>'Message',
        );
    }
}