<div class="container">

    <?
    $activeForm = $this->beginWidget('CActiveForm', array(
        'id' => $formId,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'htmlOptions' => array(
            'class' => 'form-signin',
            'role' => 'form'
        )
    ));
    ?>
    <h2 class="form-signin-heading"><?=CHtml::encode($this->pageTitle)?></h2>

    <div>
        <?
        if($form->hasErrors()){
            echo CHtml::errorSummary($form);
        }
        ?>
    </div>

    <?=$activeForm->textField($form, 'username', array(
        'class' => 'form-control',
        'placeholder' => 'Email',
        'required' => true,
        'autofocus' => true,
    ));
    ?>
    <?=$activeForm->passwordField($form, 'password', array(
        'class' => 'form-control',
        'placeholder' => 'Пароль',
        'required' => true,
    ));
    ?>
    <?=CHtml::htmlButton('Войти', array(
        'type' => 'submit',
        'class' => 'btn btn-lg btn-primary btn-block'
    ));
    ?>
    <? $this->endWidget(); ?>

</div>