<div class="list-group-item">
    <h4 class="list-group-item-heading">
        От: <?= $item->author->name ?> (<?= $item->author->email ?>) | <?= CTimestamp::formatDate('d.m.Y H:i:s', $item->created_at) ?>
    </h4>
    <p class="list-group-item-text"><?= $item->body ?></p>
</div>