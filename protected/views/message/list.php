<div class="panel panel-default">
    <div class="panel-body">
        <div class="list-group" id="message-list">
            <? foreach($list as $item): ?>
            <div class="list-group-item" id="message-row-<?= $item->id ?>">
                <h4 class="list-group-item-heading">
                    От: <?= $item->author->name ?> (<?= $item->author->email ?>) | <?= CTimestamp::formatDate('d.m.Y H:i:s', $item->created_at) ?>
                </h4>
                <p><?= $item->body ?></p>
                <p>
                <? if(!Yii::app()->user->isGuest && Yii::app()->user->id == $toUserId): ?>
                <?=CHtml::ajaxButton('Удалить',
                        CHtml::normalizeUrl(array('/message/delete')),
                        array(
                            'type' => 'POST',
                            'data' => 'id=' . $item->id,
                            'success' => 'function(response, status, request){
                                $("#message-row-'.$item->id.'").remove();
                            }',
                            'error' => 'function(data) { alert("Error: "+data); }',
                        ),
                        array(
                            'class' => 'btn btn-danger',
                            'confirm' => 'Вы действительно хотите удалить запись?',
                        )
                    )?>
                <? endif; ?>
                </p>
            </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
<? if(!Yii::app()->user->isGuest && Yii::app()->user->id != $toUserId): ?>
<div class="panel panel-default">
    <div class="panel-heading">Написать сообщение</div>
    <div class="panel-body">
        <?
        $form = $this->beginWidget('CActiveForm', array(
            'id' => $formId,
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
            'htmlOptions' => array(
                'role' => 'form'
            )
        ));
        ?>

        <div>
            <?
            if($model->hasErrors()){
                echo CHtml::errorSummary($model);
            }
            ?>
        </div>

        <?=$form->hiddenField($model, 'to_user_id', array('value' => $toUserId)); ?>
        <div class="form-group">
            <?=$form->textArea($model, 'body', array(
                'class' => 'form-control',
                'required' => true,
                'autofocus' => true,
            ));
            ?>
        </div>
        <div class="form-group">
            <?=CHtml::ajaxSubmitButton('Отправить',
                CHtml::normalizeUrl(array('/message/send')),
                array(
                    'type' => 'POST',
                    'data' => 'js:$("#message-form").serialize()',
                    'success' => 'function(response, status, request){
                        $("#message-list").prepend(response);
                    }',
                    'error' => 'function(data) { alert("Error: "+data); }',
                ),
                array(
                    'type' => 'submit',
                    'class' => 'btn btn-primary',
                )
            );
            ?>
        </div>
        <? $this->endWidget(); ?>
    </div>
</div>
<? endif; ?>